import { BaseResponse } from "./base-response"
import { Supplier } from "./supplier"

export interface SupplierNew {
  id: number,
  name: string,
  status: number,
  addDate: string,
  address: string,
}

export type GetSupplierResponse = BaseResponse & {
  data: {
    items: Supplier[]
  }
}

export type CrudSupplierResponse = BaseResponse & {
  data: string
}
