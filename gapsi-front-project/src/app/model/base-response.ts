export interface BaseResponse {
    success: boolean,
    error: string,
    millisenconds: number,
    data: any
}