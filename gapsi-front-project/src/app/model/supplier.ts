export interface Supplier {
    id: number,
    name: string,
    status: number,
    addDate: string,
    address: string,
  }