import { BaseResponse } from "./base-response";

export type AppVersion = BaseResponse & {
  data: string
}
