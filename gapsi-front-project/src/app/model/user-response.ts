import { BaseResponse } from "./base-response";

export type UserResponse = BaseResponse & {
    data: {
        userName: string
    }
  }
  