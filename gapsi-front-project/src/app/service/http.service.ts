import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HttpService<T> {

  constructor(protected http: HttpClient) { }

  getService(url: string): Observable<T> {
    return this.http.get<T>(url).pipe(
        tap(_ => this.log('fetched data')),
        catchError(this.handleError<T>('getData', null))
      );
  }

  postService(url: string, data: Object): Observable<T> {
    return this.http.post<T>(url, data).pipe(
        tap(_ => this.log('fetched data')),
        catchError(this.handleError<T>('postData', null))
      );
  }

  putService(url: string, data: Object): Observable<T> {
    return this.http.put<T>(url, data).pipe(
        tap(_ => this.log('fetched data')),
        catchError(this.handleError<T>('postData', null))
      );
  }

  deleteService(url: string): Observable<T> {
    return this.http.delete<T>(url).pipe(
        tap(_ => this.log('fetched data')),
        catchError(this.handleError<T>('postData', null))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  protected log(message: string) {
    console.log(`HTTP Service: ${message}`);
  }

}
