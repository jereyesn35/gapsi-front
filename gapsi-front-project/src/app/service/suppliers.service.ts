import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Supplier } from '../model/supplier';
import { CrudSupplierResponse, GetSupplierResponse } from '../model/suppliers-response';
import { HttpService } from './http.service';

@Injectable({ providedIn: 'root' })
export class SupplierService {

  // URL to web api
  private suppliersUrl = environment.apiUrl + '/gapsi-app-api/suppliers';
  private getSuppliersContext = '/getSuppliers';
  private addSuppliersContext = '/add';
  private editSuppliersContext = '/edit';
  private deleteSuppliersContext = '/delete';

  constructor(private supplierService: HttpService<GetSupplierResponse>) {}

  /** GET suppliers from the server */
  getAllSuppliers(): Observable<GetSupplierResponse> {
    return this.supplierService.getService(this.suppliersUrl + this.getSuppliersContext);
  }

  addOrEditSupplier(data: Supplier): Observable<CrudSupplierResponse> {
    if(data.id != 0){
      return this.supplierService.putService(this.suppliersUrl + this.editSuppliersContext, data);
    }
    return this.supplierService.postService(this.suppliersUrl + this.addSuppliersContext, data);
  }

  removeSupplier(id: number): Observable<CrudSupplierResponse> {
    return this.supplierService.deleteService(this.suppliersUrl + this.deleteSuppliersContext + '/' + id);
  }

}
