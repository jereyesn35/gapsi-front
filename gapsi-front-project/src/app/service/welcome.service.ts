import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { AppVersion } from '../model/app-version-response';
import { UserResponse } from '../model/user-response';
import { HttpService } from './http.service';


@Injectable({ providedIn: 'root' })
export class WelcomeService {

  private welcomeUrl = environment.apiUrl + '/gapsi-app-api/data/version';
  private userNameUrl = environment.apiUrl + '/gapsi-app-api/data/user';

  constructor(private appVersionService: HttpService<AppVersion>) {}

  /** GET data from the server */
  getAppVersion(): Observable<AppVersion> {
    return this.appVersionService.getService(this.welcomeUrl);
  }

  /** GET data from the server */
  getUsername(): Observable<UserResponse> {
    return this.appVersionService.getService(this.userNameUrl);
  }

}
