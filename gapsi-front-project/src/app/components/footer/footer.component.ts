import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { WelcomeService } from 'src/app/service/welcome.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {

  appVersion = '';
  
  constructor(private welcomeService: WelcomeService, private spinner: NgxSpinnerService) { 
    console.log('Build component');
  }

  ngOnInit() {
    console.log('Start component');
    this.spinner.show();
    console.log('Start loader welcome');
    this.welcomeService.getAppVersion().subscribe(response => {
      if(response !== null && response.success){
        this.appVersion = response.data.appVersion
      }
      this.spinner.hide();
    });
  }

  ngOnDestroy() {
    console.log('End component');
  }

}
