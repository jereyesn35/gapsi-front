import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { WelcomeService } from '../../service/welcome.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {
  
  candidateName = '';
  router: Router;

  constructor(router: Router, private welcomeService: WelcomeService, private spinner: NgxSpinnerService) { 
    console.log('Build component');
    this.router = router;
  }

  ngOnInit() {
    console.log('Start component welcome');
    this.spinner.show();
    console.log('Start loader welcome');
    this.welcomeService.getUsername().subscribe(response => {
      if(response !== null && response.success){
        this.candidateName = response.data.userName
      }
      this.spinner.hide();
    });
  }

  ngOnDestroy() {
    console.log('End component');
  }

  continue(): void {
    console.log('Continue to next page');
    this.router.navigate(['/dashboard']);
  }

}
