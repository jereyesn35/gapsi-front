import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Supplier } from '../../model/supplier'

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.css']
})
export class DialogOverviewDialog implements OnInit, OnDestroy {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Supplier) {}

  isEdit: boolean = false;

  ngOnInit() {
    console.log('Start component');
    if (this.data.id != 0){
      this.isEdit = true;
    }
  }

  ngOnDestroy() {
    console.log('End component');
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}