import { AfterViewInit, OnInit, OnDestroy, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { DialogOverviewDialog } from '../edit-supplier/edit-supplier.component';
import { Supplier } from '../../model/supplier';
import { SupplierService } from 'src/app/service/suppliers.service';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements AfterViewInit, OnInit, OnDestroy {
  
  displayedColumns: string[] = ['id', 'name', 'status', 'addDate', 'address', 'actions'];
  dataSource = new MatTableDataSource<Supplier>();
  @ViewChild(MatPaginator) 
  paginator: MatPaginator;

  suppliersData: Supplier[] = []

  supplier: Supplier;

  constructor(public dialog: MatDialog, private supplierService: SupplierService, private spinner: NgxSpinnerService) {
  }

  ngAfterViewInit(): void {
    console.log('Start after view component');
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    console.log('Start component');
    this.loadSuppliersData();
  }

  ngOnDestroy() : void {
    console.log('End component');
  }

  loadSuppliersData(): void {
    this.spinner.show();
    console.log('Show loader');
    this.supplierService.getAllSuppliers().subscribe(response => {
      if(response !== null && response.success){
        this.suppliersData = response.data.items;
        this.dataSource.data = this.suppliersData;
      }
      this.spinner.hide();
    })
  }

  add(): void {
    console.log('Adding data... ');
    const supplier: Supplier = {
      id: 0,
      name: '',
      status: 0,
      addDate: '',
      address: ''
    }
    this.openDialog(supplier);
  }

  edit(element: Supplier): void {
    console.log('Edit data... ' + element.id);
    this.openDialog(element);
  }

  delete(element: Supplier): void {
    console.log('Delete data... ' + element.id);
    this.deleteData(element.id);
  }

  openDialog(supplier: Supplier): void {
    const dialogRef = this.dialog.open(DialogOverviewDialog, {
      width: '30em',
      data: supplier
  });

  dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed with data' + JSON.stringify(result));
      if(result !== undefined){
        this.supplier = result;
        this.sendData();
      }
    });
  }

  sendData(){
    this.spinner.show();
    this.supplierService.addOrEditSupplier(this.supplier).subscribe(response => {
      if(response !== null && response.success){
        console.log(response.data);
      }
      this.hideAndReloadData();
    });
  }

  deleteData(id: number){
    this.spinner.show();
    this.supplierService.removeSupplier(id).subscribe(response => {
      if(response !== null && response.success){
        console.log(response.data);
      }
      this.hideAndReloadData();
    });
  }

  private hideAndReloadData(){
    this.spinner.hide();
    this.loadSuppliersData();
  }

}