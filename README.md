# Gapsi Front

## Name
Gapsi e-commerce front end.

## Getting started

Como prerrequisito, se debe tener instaladas las siguientes dependendencias para compilar el proyecto.

NodeJS

Ejecutar los siguientes pasos para iniciar la aplicación

1. Sobre la raiz del proyecto (gapsi-front-project), ejecutar el comando npm install
2. Ejecutar el comando npm start

Si no tiene ningun error en consola, el proyecto web se estara ejecutando correctamente en la direccion http://localhost:4200